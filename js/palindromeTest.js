window.onload = function()
{
        "use strict";
	var test1Result = document.getElementById("test1");
	var test2Result = document.getElementById("test2");
	var test3Result = document.getElementById("test3");
	var test4Result = document.getElementById("test4");
	var test5Result = document.getElementById("test5");
	var test6Result = document.getElementById("test6");

    var nonpalindrome = document.getElementById( "non").innerHTML;
    var smallPalindrome = document.getElementById( "small").innerHTML;
    var veryLongPalindrome = document.getElementById( "palindrome" ).innerHTML;

    //On test
    var executionStart1 = (new Date()).getTime();
    test1Result.innerHTML = "On: nonpalindrome: isPalaindrome = ";
    test1Result.innerHTML += SinderBlok.Palindrome.On( nonpalindrome ).toString();
    test1Result.innerHTML += " " + ( (new Date()).getTime() - executionStart1).toString();

    var executionStart2 = (new Date()).getTime();
    test2Result.innerHTML = "On: smallPalindrome: isPalaindrome = ";
    test2Result.innerHTML += SinderBlok.Palindrome.On( smallPalindrome ).toString();
    test2Result.innerHTML += " " + ( (new Date()).getTime() - executionStart2).toString();

    var executionStart3 = (new Date()).getTime();
    test3Result.innerHTML = "On: veryLongPalindrome: isPalaindrome = ";
    test3Result.innerHTML += SinderBlok.Palindrome.On( veryLongPalindrome ).toString();
    test3Result.innerHTML += " " + ( (new Date()).getTime() - executionStart3).toString();

    // Ologn test
    var executionStart4 = (new Date()).getTime();
    test4Result.innerHTML = "Ologn: nonpalindrome: isPalaindrome = ";
	test4Result.innerHTML += SinderBlok.Palindrome.Ologn( nonpalindrome ).toString();
	test4Result.innerHTML += " " + ( (new Date()).getTime() - executionStart4).toString();

    var executionStart5 = (new Date()).getTime();
    test5Result.innerHTML = "Ologn: smallPalindrome: isPalaindrome = ";
    test5Result.innerHTML += SinderBlok.Palindrome.Ologn( smallPalindrome ).toString();
    test5Result.innerHTML += " " + ( (new Date()).getTime() - executionStart5).toString();

    var executionStart6 = (new Date()).getTime();
    test6Result.innerHTML = "Ologn: veryLongPalindrome: isPalaindrome = ";
    test6Result.innerHTML += SinderBlok.Palindrome.Ologn( veryLongPalindrome ).toString();
    test6Result.innerHTML += " " + ( (new Date()).getTime() - executionStart6).toString();
};
