window.onload = function()
{
	"use strict";
	// create all the DOM references and local variables we'll need to instantiate exercise 1 (KeyCounts)
	var exercise1, testString, defaultString, stringParser,
		keyCountsDisplayField = document.getElementById( "keyCountsDisplay" ),
		keyCountsSubmitButton = document.getElementById( "keyCountsSubmit" ),

	// create all the DOM referneces and local variables we'll need to instantiate exercise 2 (Palindrome)
		exercise2,
		positiveMessage = document.getElementById( "positiveResult" ).innerHTML,
		positiveStyle = "result positive",
		negativeMessage = document.getElementById( "negativeResult" ).innerHTML,
		negativeStyle = "result negative",
		palindromeInputField = document.getElementById( "palindromeInput" ),
		palindromeResultElement = document.getElementById( "result" ),
		palindromeSubmitButton = document.getElementById( "submitButton" );

	// assign a value to our defaultString variable for exercise1.
	defaultString = "John,2\n\nJane,3\n\nJohn,4\n\nJane,5";

	/*
	 * This function decides how the data format represented by the defualtString method above will be parsed into
	 * value objects. It takes a single argument of type String. The function will return an array of value objects.
	 */
	stringParser = function( dataInput )
	{
		// create our local variables...
		var i, datalist, currentItem, parsedVOs = [];
		// strip all "\n" characters from the input string
		datalist = SinderBlok.StringUtils.stripNewLineChars( dataInput );
		// loop through the resulting string to validate our input data...
		for( i = 0; i < datalist.length; i++ )
		{
			// grab each element and split into an array delimited by a comma
			currentItem = datalist[ i ].split( "," );
			if(
				// if the current item doesn't have two elements...
				currentItem.length !== 2 ||
				// or the first element is not a string...
				typeof currentItem[ 0 ] !== "string" ||
				// or the second element is not a number...
				typeof parseInt( currentItem[ 1 ], 10 ) !== "number"
				)
			{
				// then our element has failed validation and can be skipped.
				continue;
			}
			// otherwise, create a new Value Object from the element, and store it in our return list.
			parsedVOs.push( new SinderBlok.ValueObject( currentItem[ 0 ], currentItem[ 1 ] ) );
		}
		// return our collection of valid VOs.
		return parsedVOs;
	};

	// create an instance of exercise1 and pass in required arguments using variables from up top.
	exercise1 = new SinderBlok.KeyCountsExercise( defaultString, stringParser, keyCountsDisplayField, keyCountsSubmitButton );

	// create an instance of exercise2 and pass in required arguments using variables from up top.
	exercise2 = new SinderBlok.PalindromeExercise( palindromeInputField, palindromeResultElement, palindromeSubmitButton );
	// configure the success and failure parameters using variables from up top.
	exercise2.setSuccessParams( positiveMessage, positiveStyle );
	exercise2.setFailureParams( negativeMessage, negativeStyle );
};



