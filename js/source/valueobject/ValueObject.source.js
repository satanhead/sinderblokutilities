var SinderBlok = SinderBlok || {};

/*
 * This is a simple value object designed to hold a key:value pair, mapped as String > Int. The public API provides
 * two simple methods used to retrieve the initial values, which are set only by passing them into the "constructor".
 * Due to JavaScript's loose typing, every effort should be made to check that valid values are being passed in
 * before this VO is used.
 */
SinderBlok.ValueObject = function( newKey, newValue )
{
	"use strict";
	var immutableKey = newKey.toString(),
		immutableValue = parseInt( newValue, 10 );

	return {
		key:function()
		{
			return immutableKey;
		},

		value:function()
		{
			return immutableValue;
		}
	};
};