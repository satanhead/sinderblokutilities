var SinderBlok = SinderBlok || {};

/*
 * This implementation represents the Palindrome exercise. It takes three arguments:
 * inputField - a reference to a DOM element that will collect the users input.
 * resultElement - a reference to a DOM element that will display the result.
 * submitButton - a reference to a button that will start the whole thingw.
 */
SinderBlok.PalindromeExercise = function( inputField, resultElement, submitButton )
{
	"use strict";
	var isPalindrome = false,
		successText = "",
		successStyle = "",
		failureText = "",
		failureStyle = "",
		resetStyle = "hidden",
		resetText = "";

	/*
	 * This method is used to reset the result display. It is called frequently during interaction. This method is
	 * private.
	 */
	function resetResult()
	{
		resultElement.className = resetStyle;
		resultElement.innerHTML = resetText;
	}

	/*
	 * This method represents the actions that are taken upon successfully finding a palindrome. This method is private.
	 */
	function onSuccess()
	{
		resultElement.innerHTML = successText;
		resultElement.className = successStyle;
	}

	/*
	 * This method represents the actions that are taken upon failing to find a palindrome. This method is private.
	 */
	function onFailure()
	{
		resultElement.innerHTML = failureText;
		resultElement.className = failureStyle;
	}

	/*
	 * This method it the event handler that overrides the submitButton.onclick handler. This method is private.
	 */
	function buttonClick()
	{
		isPalindrome = SinderBlok.Palindrome.isPalindrome( inputField.value );
		if( isPalindrome )
		{
			onSuccess();
		}
		else
		{
			onFailure();
		}
	}

	/*
	 * This method is invoked every time the contents of the input field have changed. This method is private.
	 */
	function textFieldChange( eventObject )
	{
		if( eventObject.keyCode === 13 )
		{
			buttonClick();
		}
		else
		{
			resetResult();
		}
	}

	// initialize our exercise...
	inputField.onkeyup = textFieldChange;
	resetResult();
	submitButton.onclick = buttonClick;

	// return the public API.
	return {
		/*
		 * This method is used to set the success message and style. It takes two arguments: a String representing the
		 * actual text to be displayed, and a String representing the class name that should be applied to the result
		 * element on success.
		 */
		setSuccessParams:function( newSuccessText, newSuccessStyle )
		{
			successText = newSuccessText;
			successStyle = newSuccessStyle;
		},

		/*
		 * This method is used to set the failure message and style. It takes two arguments: a String representing the
		 * actual text to be displayed, and a String representing the class name that should be applied to the result
		 * element on failure.
		 */
		setFailureParams:function( newFailureText, newFailureStyle )
		{
			failureText = newFailureText;
			failureStyle = newFailureStyle;
		}
	};
};