var SinderBlok = SinderBlok || {};

(
	SinderBlok.Palindrome =
	{
		/*
		 *	This method is used to detect a palindrome. It takes a single argument of type String. The method returns a
		 *	boolean indicating whether or not a palindrome has been found.
		 */
		isPalindrome: function ( input )
		{
			// enable ECMA5 strict
			"use strict";
			// create a variable to store the return value
			var booleanResult;
			// check the input length; if we're dealing with a very large input...
			if( input.length > 10000 )
			{
				// then use the Ologn method to detect palindrome.
				booleanResult = SinderBlok.Palindrome.Ologn( input );
			}
			else
			{
				// otherwise use the On method.
				booleanResult =  SinderBlok.Palindrome.On( input );
			}
			// return the resulting boolean
			return booleanResult;
		},

		/*
		 *	This method is a basic approach to detecting a palindrome. It takes one input of type string, and returns a
		 *	boolean that indicates whether or not a palindrome has been detected.
		 */
		On: function ( input )
		{
			// enable ECMA5 strict
			"use strict";
			// create our local variables
			var stripped, reversedInput;
			// strip the input of non-alphanumeric characters and set it to lower case
			stripped = SinderBlok.StringUtils.stripNonAlphaNumeric( input ).toLowerCase();
			// reverse the stripped, normalized input value
			reversedInput= SinderBlok.StringUtils.reverseString( stripped );
			// and compare it to the raw input value and return the resulting boolean.
			return stripped === reversedInput;
		},

		/*
		 *	This method is a basic approach to detecting a palindrome. It takes one input of type string, and returns a
		 *	boolean that indicates whether or not a palindrome has been detected.
		 */
		Ologn: function ( input )
		{
			// enable ECMA5 strict
			"use strict";
			// create local variables for this script
			var odd, stripped, inputHalfLength, halfInput, reversedHalfInput;
			// strip the input of non-alphanumeric characters and set it to lower case
			stripped = SinderBlok.StringUtils.stripNonAlphaNumeric( input ).toLowerCase();
			// determine the half-length of the input string
			inputHalfLength = stripped.length / 2;
			// determine whether the length of the input string is odd
			odd = ( stripped.length % 2 === 1 );
			// if the legth of the input string is odd, drop the decimal from the resutling inputHalfLength
			inputHalfLength = odd ? Math.floor( inputHalfLength ) : inputHalfLength;
			// grab the second half of the input string (if odd, we'll have to offset the search index - we don't care
			// about the character at the very center of the input string
			reversedHalfInput = odd ? stripped.substring( inputHalfLength + 1 ) : stripped.substring( inputHalfLength );
			// reverse the second half of the input.
			reversedHalfInput = SinderBlok.StringUtils.reverseString( reversedHalfInput );
			// grab the first half of the input
			halfInput = stripped.substring( 0, inputHalfLength );
			// compare the modified second half against the first half, and return the resulting boolean.
			return halfInput === reversedHalfInput;
		}
	}
);