var SinderBlok = SinderBlok || {};

/*
 * This implementation represents the KeyCount exercise. It takes four arguments:
 * defaultValue of type String - the value of this will be set as the defaultValue of the displayField.
 * parserFunction of type Function - a function to parse the data format, as represented by the defaultValue property.
 * displayField - a reference to a textarea element.
 * submitButton - a reference to a button.
 */
SinderBlok.KeyCountsExercise = function( defaultValue, parserFunction, displayField, submitButton )
{
	"use strict";
	var resultValue = "",
		inputValue,
		valueObjects = [],
		valueTotals = {},
		keyTotalMessage = "The total for ",
		valueTotalMessage = " is ";

	/*
	 * This mehtod is used to output data to the displayField. This method is private.
	 */
	function updateDisplay()
	{
		displayField.value = resultValue;
	}

	/*
	 * This method is used to assemble the string that will be displayed within the displayField. This method is private.
	 */
	function assembleResultValue()
	{
		var vo;
		for( vo in valueTotals )
		{
			resultValue +=
				keyTotalMessage +
				vo +
				valueTotalMessage +
				valueTotals[ vo ].toString() +
				". ";
		}
		updateDisplay();
	}

	/*
	 * This method receives value object from the external parserFunction and counts the totals for each key:value pair.
	 * This method is private.
	 */
	function parseValues()
	{
		var i, currentVO;
		// call external parser and store the resulting value objects
		valueObjects = parserFunction( inputValue );
		// loop through the value objects and calculate their totals.
		for( i = 0; i < valueObjects.length; i++ )
		{
			currentVO = valueObjects[ i ];
			if( valueTotals[ currentVO.key() ] )
			{
				valueTotals[ currentVO.key() ] +=  currentVO.value();
			}
			else
			{
				valueTotals[ currentVO.key() ] = currentVO.value();
			}
		}
		// assemble the result value
		assembleResultValue();
	}

	/*
	 * This method is the event handler that overrides the submitButton.onclick handler.
	 * This method is private.
	 */
	function buttonClick()
	{
		parseValues();
	}

	// initialize the values that have been passed into the "constructor"
	inputValue = defaultValue;
	displayField.defaultValue = inputValue;
	submitButton.onclick = buttonClick;

	// return the public API
	return {
		/*
		 * This method can be used to retrieve the exact text that is displayed in the displayField.
		 */
		getResultValue:function()
		{
			return resultValue;
		},

		/*
		 * This method can be used to set a new input value at runtime. It takes a single argument: a String representing
		 * a new set of data for the component to run against.
		 */
		setInputValue:function( newInputValue )
		{
			inputValue = newInputValue;
		},

		/*
		 * This method can be used to retrieve a the classes internal list of Value Objects. This method will return an
		 * array containing valid value objects that have been derived from the data input.
		 */
		getValueObjects:function()
		{
			return valueObjects;
		}
	};
};
