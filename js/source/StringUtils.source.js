var SinderBlok = SinderBlok || {};

SinderBlok.StringUtils =
{
	/*
	 *	This method is used to reverse a string. It takes a single argument of type String. The return type is a
	 *	String containing the reverse value of the input.
	 */
	reverseString: function ( input )
	{
		"use strict";
		// split the input string into an array, reverse the array, then rejoin the elements into a string.
		var reversedInput = input.split( "" ).reverse().join("");
		// return the resulting string.
		return reversedInput;
	},

	/*
	 *	This method removes all non-alpha numeric characters from a string. The method takes a single argument of
	 *	type String. The return value is a String containing only the alphanumeric characters that were present in
	 *	the input string.
	 */
	stripNonAlphaNumeric: function ( input )
	{
		"use strict";
		// create our local variables
		var stripped, pattern;
		// store the regex pattern we'll use for our search - in this case we are looking for all non-word characters
		// and the underscore (_) character (as it's not included as a non-word character.
		pattern = /[\W_]/g;
		// seach the string and replace all found characters with an empty string.
		stripped = input.replace( pattern, "" );
		// return the resulting string
		return stripped;
	},

	stripNewLineChars: function( input )
	{
		"use strict";
		// create local variables
		var stripped, i;
		// split the string into an array with \n as the delimiter.
		stripped = input.split( "\n" );
		// loop through the array and clean any empty indices.
		for( i = 0; i < stripped.length; i++ )
		{
			if( stripped[ i ] === "" || stripped[ i ] === undefined )
			{
				stripped.splice( i, 1 );
				i--;
			}
		}
		// return the resulting array
		return stripped;
	}
};
