# A collection of fun coding exercises

Coding exercises are both fun to do and they help keep your mind fresh for the task.
When you go long without using your skills, they fade. Like any skill, you need to
continually use it to stay frosty.

"We're all in strung out shape, but stay frosty, and alert. We can't afford to let
one of those bastards in here."
  -Cpl. Dwayne Hicks, USCMC Serial A27/TQ4.0.48215E9

When you don't continually use your skills, you allow "one of those bastards in here."
One of those bastards being, stagnation, complacency, or worst of all, the cessation
of continual learning.

The following exercises are a collection of exercises that either I was asked to
complete by a third party, or that I felt like publishing on my own. I hope that the
examples herein contain similar value to you as completing them did to me.

In the source code of the examples below, viewers will notice a few things: firstly, I like my code to be
human readable. My namespaces, variable names, function names, etc are decidedly verbose for JavaScript. In
a production environment, and were file size an issue, the production scripts can be obfuscated for live use.
In this case the scripts are mostly all very small, and readability was more important to me than download
times. Secondly, I specifically did not use any fancy tools, frameworks, etc. I wanted viewers to be seeing
the raw, raw code. I very specifically did not use a source import management tool like require.js, nor did
I use any frameworks or helper libraries, like JQuery or Angular - I wanted the code to be readable for what
it is. Finally, my code formatting is not like what most JavaScript developers use - again, I places emphasis
on readability as opposed to getting lost in vines of curly-braces and parentheses.

* Source: [https://bitbucket.org/satanhead/sinderblokutilities](https://bitbucket.org/satanhead/sinderblokutilities)
* Live View: [http://sinderblok.com/filez/SinderBlokUtils/](http://sinderblok.com/filez/SinderBlokUtils/)
* Twitter: [@HRCluffNStuff](http://twitter.com/HRCluffNStuff)

## Key Count Exercise

The Key Counts exercise is designed to highlight how a developer might work dynamically with DOM elements using
JavaScript, and handle parsing data from uncertain sources. The exercise makes use of statically rendered page elements,
and JavaScript for interactivity.

For this exercise, I chose to treat my data source as I would a service, or external data source that's not
within my direct control. With an external data source, one would expect the data supplier to have a hand in
the means by which the data is interpreted and injected into the system I'm building. Consequently, when
initializing the this exercise I expressly set a parsing function as one of the arguments to be passed in. I
also created a value object to receive the parsed data so that the system I built was working with an
immutable data source. See the source code for additional details. This implementation relies on this Value
Object and some of the String helper methods that I'd written. The implementation of the exercise itself
takes the form of a closure, so that it's initial properties remain private an immutable, and it's inner-
workings are largely hidden. The implementation also provides a minimal public API.


## Palindrome Detection

Palindrome detection is classic CompSci exercise. The goal is to find the most efficient method of detecting a
palindrome given a specific programming language. A palindrome is defined as a word, phrase, number, or other sequence
of units that may be read the same way in either direction, with general allowances for adjustments to punctuation and
word dividers.

Using Javascript, the simplest and most straight-forward means of verifying a palindrome is reversing
a string and comparing the two. Using Big O notation, this would be O(n). To accomplish this, we need
a means of normalizing a string, and reversing it. The class
<span class="code">SinderBlok.StringUtils</span> contains two methods:
<span class="code">reverseString</span>, which simply splits the string into an array, reverses it,
joins the array elements, and returns the resulting string. The second method,
<span class="code">stripNonAlphaNumeric</span> uses a regular expression to remove all non-word
characters and any instances of the underscore character (because "_" is not included in \W), and
returns the resulting string. Both of these methods are use within the class
<span class="code">SinderBlok.Palindrome</span>, where the comparison is made.

Again, a palindrome is a phrase that reads the same in either direction - so, one must ask, "wouldn't
we save computation time if we reversed only half our input then compared it to the other half?" or
O(log n)? This might give us an edge when dealing with
<a href="http://norvig.com/pal2txt.html" target="_blank">The longest palindromic sentence on earth</a>.
The answer is "yes," however, the result is practice is perhaps not as profound as we may like. In
the class <span class="code">SinderBlok.Palindrome</span>, there are two methods that are used to
identify a palindrome: <span class="code">On</span>, and <span class="code">Ologn</span>. The first
uses the straight-forward means described in the paragraph above. The second, however, reverses only
half our input, then compares it to the other half. Surprisingly, this does give us a noticeable
decrease in execution time when using a really massive palindrome, like the one identified above.
However, the optimization only really gives us an advantage of 1-2 milliseconds depending on the
browser the script is executed in. Sadly, someone has yet to write a palindrome that is long enough
for this approach to have any real benefit. Summation? The simplest solution, is often the best
solution describes this case perfectly.

Again, as with the above exercise, the implementation of the exercise itself takes the form of a closure, with initial
variables and properties being private and immutable, but also relies on some additional utility classes that are also
part of this project.